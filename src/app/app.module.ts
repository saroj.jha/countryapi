import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { CountryComponent } from './country/country.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppRoutingModule } from './app-routing.module';
import { CountrydetailsComponent } from './countrydetails/countrydetails.component';
import {AgGridModule} from 'ag-grid-angular';





@NgModule({
  declarations: [
    AppComponent,
    CountryComponent,
    CountrydetailsComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule,
    AppRoutingModule,
    AgGridModule.withComponents(null),
    
    
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
