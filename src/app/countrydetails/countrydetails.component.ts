import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import{ActivatedRoute, Router, Params} from '@angular/router';
import { CountryserviceService } from '../countryservice.service';


export class Country{

  constructor(
    
     name: string,
     
  ) {
  }

}

@Component({
  selector: 'app-countrydetails',
  templateUrl: './countrydetails.component.html',
  styleUrls: ['./countrydetails.component.css']
})
export class CountrydetailsComponent implements OnInit {

  // constructor(private route: Router) { }

  // ngOnInit(): void {
  // }
  name;
  Countrys: Country[];
 // name: string;
 private number: any;

  post;
  countryDetail;
  bor: any;
  bod;


  br = [];
  lan = [];
  
   
  
   Country:any;
  country=[];
  constructor(private httpClinet : HttpClient, 
              private restservice : CountryserviceService,
              private route: Router,
              private rout:ActivatedRoute
              ){  }

  ngOnInit(): void {

     this.number = this.rout.snapshot.paramMap.get('num');
     this.getdata();
    

  }
    

    private getdata() {

      this.restservice.getCountryData().subscribe(data => {
        this.post = data;
        const countryCode = this.number;
        for (let i = 0; i < this.post.length; i++) {
          if (this.post[i].alpha3Code === countryCode) {
            console.log(this.post[i]);
            this.countryDetail = this.post[i];
            this.lan = this.countryDetail.languages;
          }
        }
        this.bod = this.countryDetail.borders;
        const border = this.countryDetail.borders;
        this.br = this.post.filter(function(a) {
          return border.indexOf(a.alpha3Code) >= 0;
        });
      });
    }
    
      
    
       search(){
         if(this.name ==""){
           this.ngOnInit();
         }else{
          this.Country =this.Country.filter(res =>{
            return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
          });
        
      }
    }

  
}
