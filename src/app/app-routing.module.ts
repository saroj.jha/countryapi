import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CountryComponent } from './country/country.component';
import {CountrydetailsComponent} from './countrydetails/countrydetails.component';

const routes: Routes = [
  {path:'', component:CountryComponent},
  { path: 'countrydetails/:num', component: CountrydetailsComponent }
  
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes),
    CommonModule],
    exports: [RouterModule],
    providers:[]
  
})
export class AppRoutingModule { }
